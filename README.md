# Arduichatière #

Voir : 
https://annuel2.framapad.org/p/ArduiChatiere

Nous avons deux sketches Arduino: 
* InicializarLLaves, qui permet d'écrire les noms des chats sur les chips RFID,
* Chatiere, qui est le programme de fontionnement normal de la chatière.

Il faut commencer par actualiser le tableau des noms de chats dans les deux fichiers .ino 

Après en se connectant par le port USB de l'arduino, il faut téleverser le sketch InicializarLlaves, à partir de la console série, on donne les numéros des chats puis on présente la clé RFID pour y écrires le nom. 

Ensuite, on téléverse le deuxième sketch, Chatière, et on met la chatière en production.
