/*
 * Arduichatiere - v0.2 - https://framagit.org/ulvida/arduichatiere
 * 
 * (C) Copyright 2018 - Daniel Viñar Ulriksen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 *
 */
//////////////////////////// librerías necesarias //////////////////////////////

// El bus de comunicación con el lector de RFID: MFRC522
#include <SPI.h>

// La librería del lectro RFID, MFRC522 
/* * ----------------------------------------------------------------------------
 * See: MFRC522 library examples https://github.com/miguelbalboa/rfid
 * for further details and other examples.
 * *
 * Typical pin layout used:
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
 */
#include <MFRC522.h>

// La librería del Servo
#include <Servo.h>

// Wire 
#include <Wire.h>  // Comes with Arduino IDE

// Librería para la pantalla LCD con interfaz I2C
// Get the LCD I2C Library here: 
// https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
#include <LiquidCrystal_I2C.h>

/*
  For visualizing whats going on hardware we need some leds and to control door lock a relay and a wipe button
  (or some other hardware) Used common anode led,digitalWriting HIGH turns OFF led Mind that if you are going
  to use common cathode led or just seperate leds, simply comment out #define COMMON_ANODE,
*/

// #define COMMON_ANODE

#ifdef COMMON_ANODE
#define LED_ON LOW
#define LED_OFF HIGH
#else
#define LED_ON HIGH
#define LED_OFF LOW
#endif

constexpr uint8_t redLed = 7;   // Set Led Pins
constexpr uint8_t greenLed = 6;
constexpr uint8_t blueLed = 5;

// Create MFRC522 instance.
constexpr uint8_t RST_PIN = 9;     // Configurable, see typical pin layout above
constexpr uint8_t SS_PIN = 10;     // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

MFRC522::MIFARE_Key key;

Servo puerta;

constexpr uint8_t pinPuerta = 8; 
constexpr uint8_t posAbierta = 0; 
constexpr uint8_t posCerraada = 90; 

// La pantalla LED en I2C
// Con un programita ad hoc detectamos que nuestra LCD tiene I2C address 0x3F
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
// (esta configuación queda por documentar, pero funciona)
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
byte lineaLCD = 3;   //para que la línea siguiente sea la 0 inicializamos a 3 (la última línea)

// Los nombres y las claves de los gatos
struct gato {
     char nombre[16];
     byte clave[6];
     byte UID[4]; 
  } gatos[]= {
    {"Margaux", NULL, NULL},
    {"Euclides", NULL, NULL},
    {"Tobías", NULL, NULL},
    {"Scheherazade", NULL, NULL},
    {"Ernesto", NULL, NULL}
  };

int cantidadGatos = div(sizeof(gatos),sizeof(gatos[0])).quot;

gato ultimoGato = {NULL, NULL, NULL};
int i_ultimoGato = 0;


// Declaración de la variable status y otros
byte sector         = 1;
byte trailerBlock   = 7;
byte blockAddr      = 4;
byte dataBlock[]    = {
    0x01, 0x02, 0x03, 0x04, //  1,  2,   3,  4,
    0x05, 0x06, 0x07, 0x08, //  5,  6,   7,  8,
    0x09, 0x0a, 0xff, 0x0b, //  9, 10, 255, 11,
    0x0c, 0x0d, 0x0e, 0x0f  // 12, 13, 14, 15
}; // Estos datos no los usamos

uint8_t successRead;    // Variable integer to keep if we have Successful Read from Reader
byte readCard[4];   // Stores scanned ID read from RFID Module

void parpadearLEDs (int I=2) {
  digitalWrite(redLed, LED_ON);  // Blink leds
  digitalWrite(greenLed, LED_ON); 
  digitalWrite(blueLed, LED_ON);
  lcd.noBacklight(); 
  for(int i=0;i<I;i++) {
    delay(200);
    digitalWrite(redLed, LED_OFF);  
    digitalWrite(greenLed, LED_OFF);
    digitalWrite(blueLed, LED_OFF);
    lcd.backlight();
    delay(1200);
    digitalWrite(redLed, LED_ON);  // 
    digitalWrite(greenLed, LED_ON); 
    digitalWrite(blueLed, LED_ON); 
    lcd.noBacklight(); 
  }
  digitalWrite(redLed, LED_OFF);  
  digitalWrite(greenLed, LED_OFF);
  digitalWrite(blueLed, LED_OFF);
  lcd.backlight(); 
}


void setup() {
  //Arduino Pin Configuration
  pinMode(redLed, OUTPUT);
  pinMode(greenLed, OUTPUT);
  pinMode(blueLed, OUTPUT);
  
  //Protocol Configuration
  Serial.begin(9600);  // Initialize serial communications with PC
  SPI.begin();           // MFRC522 Hardware uses SPI protocol
  mfrc522.PCD_Init();    // Initialize MFRC522 Hardware
  //If you set Antenna Gain to Max it will increase reading distance
  mfrc522.PCD_SetAntennaGain(mfrc522.RxGain_max);
  lcd.begin(20,4);   // initialización de la pantalla LCD (20 car x 4 lineas)

  Serial.println(F("Contrôle de Chatière v0.2"));   // For debugging purposes
  ShowReaderDetails();  // Show details of PCD - MFRC522 Card Reader details
  //-------- Write characters on the display ------------------
  // NOTE: Cursor Position: (CHAR, LINE) start at 0  
  lcd.setCursor(0,0);
  lcd.print("Arduigatuno, hola!");
  lcd.setCursor(0,1); 
  lcd.print("Arrancando...");
  parpadearLEDs (3);

  // In this sample we use the second sector,
  // that is: sector #1, covering block #4 up to and including block #7

  // Prepare the key (used both as key A and as key B)
  // using FFFFFFFFFFFFh which is the default at chip delivery from the factory
  for (byte i = 0; i < 6; i++) {
      key.keyByte[i] = 0xFF;
  }
  // Inicialiar el servo que blouea y abre la puerta
  lcd.clear();
  String especiales = "áéíóúéêèàñÑÁÉÚÍ¿¡#$%&?/()[]{}";
  utf8ascii(especiales);  //tentativa fracasada de ASCII extendido
  lcd.print(especiales); // lo dejo porque al menos pone un car, no 2. 
  parpadearLEDs (4);
  
  puerta.attach(pinPuerta);
  // y asegurarse que está cerrada
  puerta.write(posCerraada);
  lcd.home();
  lcd.clear();
  // lcd.autoscroll();   // anda mal
  lcd.noBacklight();
} 

void loop() {
do {
    successRead = getID();  // sets successRead to 1 when we get read from reader otherwise 0
    digitalWrite(blueLed, LED_ON);    // Visualize Master Card need to be defined
    delay(200);
  digitalWrite(redLed, LED_OFF);  // Make sure led is off
  digitalWrite(greenLed, LED_OFF);  // Make sure led is off
    digitalWrite(blueLed, LED_OFF);
    delay(200);
 } while (!successRead);   //the program will not go further while you are not getting a successful read

  Serial.print("i_ultimoGato:"); Serial.println(i_ultimoGato);
  Serial.print("ultimoGato.nombre:"); Serial.println(ultimoGato.nombre);

  if ( i_ultimoGato < 0 ) {
    Serial.println(F("Acceso rechazado!!!"));
    digitalWrite(redLed, LED_ON);
    delay(500);
    digitalWrite(redLed, LED_OFF);
    return;
  }
  else abrirPuerta(5000);
}

///////////////////////////////////////// Get PICC's UID ///////////////////////////////////
uint8_t getID() {
  // Getting ready for Reading PICCs
  if ( ! mfrc522.PICC_IsNewCardPresent()) { //If a new PICC placed to RFID reader continue
    return 0;
  }
  if ( ! mfrc522.PICC_ReadCardSerial()) {   //Since a PICC placed get Serial and continue
    return 0;
  }
  Serial.println(F("UID del PICC escaneado:"));
  dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
  Serial.println("");

  MFRC522::StatusCode status;
  byte buffer[18];
  char nombre[18];
  byte size = sizeof(buffer);
  int i = 0;

  // Authenticate using key A
  Serial.println(F("Autenticando usando la clave A"));
  status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
      digitalWrite(redLed, LED_ON);
      Serial.print(F("PCD_Authenticate() failed: "));
      Serial.println(mfrc522.GetStatusCodeName(status));
  } else 
    Serial.println(F("Autenticado correctamente con la clave A"));
    
  Serial.print(F("Leyendo el nombre del gato..."));
  status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, buffer, &size);
  if (status != MFRC522::STATUS_OK) {
      digitalWrite(redLed, LED_ON);
      Serial.print(F("MIFARE_Read() failed: "));
      Serial.println(mfrc522.GetStatusCodeName(status));
  } else {
      strncpy(nombre, buffer,18);
      Serial.println(F("Éxito en la lectura de bloque del nombre"));
  }
  Serial.print(F("Datos en bloque ")); Serial.print(blockAddr); Serial.println(F(":"));
  dump_byte_array(buffer, 16); Serial.println();
  Serial.println(nombre);
  Serial.println();

  for(i=0;i<cantidadGatos;i++) {
    // Serial.print(nombre); Serial.println(gatos[i].nombre);
    if(strcmp(nombre,gatos[i].nombre) == 0) {
      strcpy(ultimoGato.nombre, gatos[i].nombre);
      i_ultimoGato = i;
      break;
    }
  }
  if(i == cantidadGatos) { // no se encontró el nombre en a tabla de gatos
    i_ultimoGato = -1;
  }

  mfrc522.PICC_HaltA(); // Stop reading
  // Stop encryption on PCD
  mfrc522.PCD_StopCrypto1();
  return 1;
}

/////////////////////////////////////////  Access Granted    ///////////////////////////////////
void abrirPuerta ( uint16_t setDelay) {
  Serial.println(F("Abriendo puerta!!!"));
 
  digitalWrite(blueLed, LED_OFF);   // Turn off blue LED
  digitalWrite(redLed, LED_OFF);  // Turn off red LED
  digitalWrite(greenLed, LED_ON);   // Turn on green LED

  lcd.backlight();

  utf8ascii(ultimoGato.nombre);
  imprimirLinea(ultimoGato.nombre);
  /*
  if (++lineaLCD==4){
    lineaLCD=0;
    lcd.clear();
  }
  lcd.setCursor(0, lineaLCD);

  lcd.print(ultimoGato.nombre);
  lcd.print(" / ");
  */
  
  // digitalWrite(relay, LOW);     // Unlock door!
  puerta.write(posAbierta);       //activar el servo a posición abierta
  
  delay(setDelay);          // Hold door lock open for given seconds
  // digitalWrite(relay, HIGH);    // Relock door
  puerta.write(posCerraada);       //activar el servo a posición cerrada
  delay(1000);            // Hold green LED on for a second
  digitalWrite(greenLed, LED_OFF);   // Turn on green LED
  lcd.noBacklight();
}

/**
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}
/*
 * Mostrar la info del lector de RFID
 */
void ShowReaderDetails() {
  // Get the MFRC522 software version
  byte v = mfrc522.PCD_ReadRegister(mfrc522.VersionReg);
  Serial.print(F("MFRC522 Software Version: 0x"));
  Serial.print(v, HEX);
  if (v == 0x91)
    Serial.print(F(" = v1.0"));
  else if (v == 0x92)
    Serial.print(F(" = v2.0"));
  else
    Serial.print(F(" (unknown),probably a chinese clone?"));
  Serial.println("");
  // When 0x00 or 0xFF is returned, communication probably failed
  if ((v == 0x00) || (v == 0xFF)) {
    Serial.println(F("WARNING: Communication failure, is the MFRC522 properly connected?"));
    Serial.println(F("SYSTEM HALTED: Check connections."));
    // Visualize system is halted
    digitalWrite(greenLed, LED_OFF);  // Make sure green LED is off
    digitalWrite(blueLed, LED_OFF);   // Make sure blue LED is off
    digitalWrite(redLed, LED_ON);   // Turn on red LED
    while (true); // do not go further
  }
}


void imprimirLinea(char *stringLinea) {
  if (++lineaLCD==4){
    lineaLCD=0;
    lcd.clear();
  }
  lcd.setCursor(0, lineaLCD);

  lcd.print(stringLinea);
  lcd.print(" / ");
}

// ****** UTF8-Decoder: convert UTF8-string to extended ASCII *******
static byte c1;  // Last character buffer

// Convert a single Character from UTF8 to Extended ASCII
// Return "0" if a byte has to be ignored
byte utf8ascii(byte ascii) {
    if ( ascii<128 )   // Standard ASCII-set 0..0x7F handling  
    {   c1=0;
        return( ascii );
    }

    // get previous input
    byte last = c1;   // get last char
    c1=ascii;         // remember actual character

    switch (last)     // conversion depending on first UTF8-character
    {   case 0xC2: return  (ascii);  break;
        case 0xC3: return  (ascii | 0xC0);  break;
        case 0x82: if(ascii==0xAC) return(0x80);       // special case Euro-symbol
    }

    return  (0);                                     // otherwise: return zero, if character has to be ignored
}

// convert String object from UTF8 String to Extended ASCII
String utf8ascii(String s)
{      
        String r="";
        char c;
        for (int i=0; i<s.length(); i++)
        {
                c = utf8ascii(s.charAt(i));
                if (c!=0) r+=c;
        }
        return r;
}

// In Place conversion UTF8-string to Extended ASCII (ASCII is shorter!)
void utf8ascii(char* s)
{      
        int k=0;
        char c;
        for (int i=0; i<strlen(s); i++)
        {
                c = utf8ascii(s[i]);
                if (c!=0)
                        s[k++]=c;
        }
        s[k]=0;
}
